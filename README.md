# PHP WEBAPP : Apache + PHP7

FROM php:apache

PHP version 7.3
APACHE version 2.4

`php -m`
```
[PHP Modules]
bcmath, Core, ctype, curl, date, dom, fileinfo, filter, ftp, gd, gettext, hash, iconv, intl, json, libxml, mbstring, mongodb, mysqlnd, openssl, pcre, PDO, pdo_mysql, pdo_sqlite, Phar, posix, readline, Reflection, session, SimpleXML, SPL, sqlite3, standard, tokenizer, xml, xmlreader, xmlwriter, Zend OPcache, zip, zlib
[Zend Modules]
Zend OPcache
```

For PHP DOCS see https://hub.docker.com/r/amsdard/php/

## using - quick HOWTO

**./docker/webapp/000-default.conf**
```
<VirtualHost *:80>
    ServerName localhost
    ServerAdmin admin@localhost
    DocumentRoot /opt/public
    DirectoryIndex index.php

    # Available loglevels: trace8, ..., trace1, debug, info, notice, warn,
    # error, crit, alert, emerg.
    # It is also possible to configure the loglevel for particular
    # modules, e.g.
    #LogLevel info ssl:warn

    #Include conf-available/docker-php.conf
    #Include conf-available/security.conf

    <Directory /opt/public>
        AllowOverride All
        Order allow,deny
        Allow from all
        Require all granted
    </Directory>
</VirtualHost>
```

**index.php**
```
<?php phpinfo();
```
 * keep in mind You should use `$_ENV` to get container's environment variables. `$_SERVER` will not contain container ENV (see also apache `SetEnv` and `PassEnv` rules)

**Dockerfile**
```
FROM amsdard/php-webapp

COPY ./docker/webapp/000-default.conf /etc/apache2/sites-enabled/000-default.conf

COPY ./index.php /opt/public/index.php
```
