#!/bin/bash

UNAME="www-data"
OPT_UID=`stat -c "%u" /opt`
OPT_GID=`stat -c "%g" /opt`
CMD_ROOT="apache2-foreground ${RUN_AS_ROOT}"

## make the same home for root as for user
if [ ! -L /root ]; then
    mv /root /usr
    ln -s /var/www /root
fi

## UID mapping
if [[ "$OPT_UID" -gt "0" ]]; then
    usermod -u $OPT_UID $UNAME >/dev/null 2>&1
    groupmod -g $OPT_GID $UNAME >/dev/null 2>&1
    export HOME=/var/www
    chown $UNAME:$UNAME -R $HOME >/dev/null 2>&1
fi

## run as user if required
if [[ "$OPT_UID" -gt "0" && "$(grep -c -E "(^| )${1}( |$)" <<< "$CMD_ROOT")" -eq "0" ]]; then
    sudo -E -u $UNAME -g $UNAME $@
else
    exec "$@"
fi